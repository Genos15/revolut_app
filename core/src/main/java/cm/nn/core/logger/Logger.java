package cm.nn.core.logger;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.Arrays;

import cm.nn.core.BuildConfig;

public final class Logger {


    public static void show(Object data) {
        if (data == null) return;
        if (data instanceof Number) {
            Log.println(Log.ERROR, BuildConfig.APPLICATION_ID, String.valueOf(data));
        } else if (data instanceof Throwable) {
            ((Throwable) data).printStackTrace();
        } else if (data instanceof Array) {
            Log.println(Log.ERROR, BuildConfig.APPLICATION_ID, Arrays.toString((Object[]) data));
        } else {
            Log.println(Log.ERROR, BuildConfig.APPLICATION_ID, data.toString());
        }
    }

    public static void ignore(Object... object) {
    }
}
