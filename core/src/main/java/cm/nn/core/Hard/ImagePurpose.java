package cm.nn.core.Hard;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import cm.nn.core.logger.Logger;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ImagePurpose {

    private static Disposable cleaner;

    public static void ApplyImage(@NonNull ImageView view, @NonNull String content, @NonNull Context context) {
        cleaner = Observable.just(context)
                .map(ctx -> {
                    Bitmap bitmap = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888);
                    Resources resources = context.getResources();
                    float scale = resources.getDisplayMetrics().density;
                    Canvas canvas = new Canvas(bitmap);

                    Paint background = new Paint();
                    background.setColor(Color.BLACK);
                    background.setStyle(Paint.Style.FILL);
                    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, 75, background);

                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setColor(Color.rgb(255, 255, 255));
                    paint.setTextSize((int) (14 * scale));
                    paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

                    Rect bounds = new Rect();
                    paint.getTextBounds(content, 0, content.length(), bounds);

                    int x = (bitmap.getWidth() - bounds.width()) / 2;
                    int y = (bitmap.getHeight() + bounds.height()) / 2;
                    canvas.drawText(content, x, y, paint);
                    return bitmap;
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(view::setImageBitmap)
                .subscribe(Logger::ignore, Logger::show);
    }

    @Override
    protected void finalize() throws Throwable {
        if (cleaner != null && !cleaner.isDisposed()) cleaner.dispose();
        super.finalize();
    }
}
