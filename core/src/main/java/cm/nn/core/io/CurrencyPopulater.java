package cm.nn.core.io;

import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cm.nn.core.binder.model.Rate;
import cm.nn.core.binder.repository.CurrencyRepository;
import cm.nn.core.io.Performer.IOClient;
import cm.nn.core.logger.Logger;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CurrencyPopulater {

    private static Disposable cleaner;

    private static Rate BASE;

    public static Map<String, String> locales = new HashMap<>();

    public static void Populating() {

        Logger.show(BASE);

        cleaner = IOClient
                .instance
                .retrofit
                .create(CurrencyLinker.class)
                .GET_CURRENCIES(BASE.getKey())
                .doOnNext(Logger::show)
                .subscribeOn(Schedulers.io())
                .subscribe(CurrencyRepository.instance::setData, Logger::show);
    }

    @Override
    protected void finalize() throws Throwable {
        if (cleaner != null && !cleaner.isDisposed()) cleaner.dispose();
        super.finalize();
    }

    public static void setBASE(Rate _BASE) {
        BASE = _BASE;
    }

    public static Rate getBASE() {
        return BASE;
    }

    static {
        BASE = new Rate("EUR", 1.d);
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                Currency currency = Currency.getInstance(locale);
                locales.put(currency.getCurrencyCode(), currency.getDisplayName());
            } catch (Exception ignored) {}
        }
    }
}
