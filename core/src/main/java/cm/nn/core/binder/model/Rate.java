package cm.nn.core.binder.model;


import androidx.annotation.NonNull;


public class Rate {

    private String key;
    private double value;

    public Rate(String _key, double _value) {
        this.key = _key;
        this.value = _value;
    }

    public String getKey() {
        return key;
    }

    public double getValue() {
        return value;
    }

    boolean isValid() {
        return key != null && value > 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "Rate{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
