package cm.nn.core.binder.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;


public class Currency {
    private String base;
    private Map<Object, Double> rates = null;

    public Currency() {
    }

    public Currency(String _base, Map<Object, Double> map) {
        this.base = _base;
        this.rates = map;
    }

    public List<Rate> getRates() {
        if (rates == null) return new ArrayList<>();

        Stack<Rate> stack = new Stack<>();
        if (!rates.containsKey(base)) {
            stack.push(new Rate(base, 1.d));
        }
        for (Map.Entry<Object, Double> entry : rates.entrySet()) {
            Rate rate = new Rate(entry.getKey().toString(), entry.getValue());
            if (rate.isValid()) {
                stack.push(rate);
            }
        }
        return stack;
    }

    @NonNull
    public Map<Object, Double> getSources() {
        if (rates == null) return new HashMap<>();
        return rates;
    }

    @NonNull
    @Override
    public String toString() {
        return "Currency{" +
                "base='" + base + '\'' +
                ", rates=" + rates +
                '}';
    }
}
