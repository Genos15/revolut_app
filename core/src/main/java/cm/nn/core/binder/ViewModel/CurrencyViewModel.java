package cm.nn.core.binder.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import cm.nn.core.binder.model.Currency;
import cm.nn.core.binder.repository.CurrencyRepository;

public class CurrencyViewModel extends ViewModel {

    private LiveData<Currency> data;

    public CurrencyViewModel() {
        data = CurrencyRepository.instance.getData();
    }

    public LiveData<Currency> getData() {
        return data;
    }

}
