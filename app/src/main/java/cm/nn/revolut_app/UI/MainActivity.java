package cm.nn.revolut_app.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import cm.nn.core.binder.ViewModel.CurrencyViewModel;
import cm.nn.revolut_app.Adapter.CurrencyAdapter;
import cm.nn.revolut_app.R;
import cm.nn.revolut_app.State.OnTextChange;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        box = new ProgressDialog(this);
        box.setTitle(getResources().getString(R.string.loading));
        box.show();
        RecyclerView recycler = findViewById(R.id.currency_recycler);
        recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        recycler.setNestedScrollingEnabled(false);
        recycler.setHasFixedSize(false);
        CurrencyAdapter adapter = new CurrencyAdapter();
        recycler.setAdapter(adapter);
        CurrencyViewModel viewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);
        viewModel.getData().observe(this, currency -> {
            adapter.setOriginal(currency);
            if (box != null && box.isShowing() && currency != null) {
                box.dismiss();
            }
        });
        adapter.setListener(position -> {
            if (position == RecyclerView.NO_POSITION) return;
            String key = "";
            if (adapter.getItemCount() > 2) {
                for (int it = 0; it < adapter.getItemCount(); it++) {
                    RecyclerView.ViewHolder holder = recycler.findViewHolderForAdapterPosition(it);
                    if (holder != null) {
                        View owner = holder.itemView;
                        if (it == position) {
                            owner.<EditText>findViewById(R.id.et_currency_value).setEnabled(true);
                            owner.<EditText>findViewById(R.id.et_currency_value).setClickable(true);
                            owner.<EditText>findViewById(R.id.et_currency_value).requestFocus();
                            TextView view = owner.findViewById(R.id.tv_currency_short_name);
                            if (view != null) {
                                key = view.getText().toString();
                                view.setOnEditorActionListener(new OnTextChange(key, adapter.getEditer()));
                            }
                        } else {
                            owner.findViewById(R.id.et_currency_value).setEnabled(false);
                            owner.findViewById(R.id.et_currency_value).setClickable(false);
                        }
                    }
                }
            }
            adapter.set_top(position, key);
            NestedScrollView scroller = findViewById(R.id.scroller_currencies);
            if (scroller != null) {
                scroller.fullScroll(View.SCROLL_INDICATOR_TOP);
            }
        });
        adapter.setEditer(sequence -> adapter.getFilter().filter(sequence));
    }
}
