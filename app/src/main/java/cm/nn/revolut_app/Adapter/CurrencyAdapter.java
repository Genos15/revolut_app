package cm.nn.revolut_app.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import cm.nn.core.Hard.ImagePurpose;
import cm.nn.core.binder.model.Currency;
import cm.nn.core.binder.model.Rate;
import cm.nn.core.io.CurrencyPopulater;
import cm.nn.core.logger.Logger;
import cm.nn.revolut_app.R;
import cm.nn.revolut_app.State.OnTextChange;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {

    private Context context;
    private Currency editables = new Currency();
    private Currency original = new Currency();
    private OnCurrencyClick listener;
    private OnCurrencyEdit editer;

    @NonNull
    @Override
    public CurrencyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_currency, parent, false);
        return new CurrencyHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyHolder holder, int position) {
        if (position == RecyclerView.NO_POSITION) return;
        if (editables == null || holder.et_value == null) return;
        Rate rate = editables.getRates().get(position);
        if (position == 0) {
            holder.et_value.setEnabled(true);
            holder.et_value.setClickable(true);
        } else {
            holder.et_value.setEnabled(false);
            holder.et_value.setClickable(false);
        }
        holder.et_value.setOnEditorActionListener(new OnTextChange(String.valueOf(rate.getKey()), editer));
        holder.tv_short_name.setText(String.valueOf(rate.getKey()));
        String value = String.format(Locale.getDefault(), "%.4f", rate.getValue());
        holder.et_value.setText(value);
        if (CurrencyPopulater.locales.size() > 0) {
            if (CurrencyPopulater.locales.containsKey(rate.getKey())) {
                holder.tv_full_name.setText(CurrencyPopulater.locales.get(rate.getKey()));
            }
        }
        ImagePurpose.ApplyImage(holder.iv_flag, rate.getKey(), context);
    }

    public void setOriginal(Currency original) {
        this.original = original;
        if (this.editables.getSources().size() == 0) {
            this.editables = original;
        }
        notifyDataSetChanged();
    }

    private Rate[] getRates() {
        return original.getRates().toArray(new Rate[0]);
    }

    private Map<Object, Double> getOriginalSource() {
        return original.getSources();
    }

    private void setEditables(Currency _editables) {
        this.editables = _editables;
        notifyDataSetChanged();
    }

    public void setListener(OnCurrencyClick listener) {
        this.listener = listener;
    }

    public void setEditer(OnCurrencyEdit editer) {
        this.editer = editer;
    }

    public void set_top(int position, String key) {
        if (editables != null && editables.getRates().size() > 2 && position < editables.getRates().size()) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Arrays.stream(getRates())
                        .filter(r -> r.getKey().contentEquals(key))
                        .findFirst()
                        .ifPresent(CurrencyPopulater::setBASE);
            } else {
                for (int it = 0; it < getRates().length; it++) {
                    if (getRates()[it].getKey().contentEquals(key)) {
                        CurrencyPopulater.setBASE(getRates()[it]);
                        break;
                    }
                }
            }
            notifyItemMoved(position, 0);
        }
    }

    @Override
    public int getItemCount() {
        return editables.getRates().size();
    }

    class CurrencyHolder extends RecyclerView.ViewHolder {
        ImageView iv_flag;
        TextView tv_short_name, tv_full_name;
        EditText et_value;

        CurrencyHolder(@NonNull View view, OnCurrencyClick listener) {
            super(view);
            iv_flag = view.findViewById(R.id.iv_flag);
            tv_short_name = view.findViewById(R.id.tv_currency_short_name);
            tv_full_name = view.findViewById(R.id.tv_currency_full_name);
            et_value = view.findViewById(R.id.et_currency_value);
            view.setOnClickListener(v -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onClick(position);
                    }
                }
            });
        }
    }

    public interface OnCurrencyClick {
        void onClick(int position);
    }

    public interface OnCurrencyEdit {
        void onEdit(CharSequence sequence);
    }

    public Filter getFilter() {
        return filter;
    }

    public OnCurrencyEdit getEditer() {
        return editer;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence sequence) {
            double initial = CurrencyPopulater.getBASE().getValue();
            double coming = sequence.length() == 0 ? 0 : Double.valueOf(sequence.toString());
            Map<Object, Double> forResult = new LinkedHashMap<>();
            if (getOriginalSource().size() > 0) {
                forResult.put(CurrencyPopulater.getBASE().getKey(), coming);
                for (Map.Entry<Object, Double> entry : getOriginalSource().entrySet()) {
                    double natural = entry.getValue();
                    forResult.put(entry.getKey(), (coming / initial) * natural);
                }
            }
            FilterResults results = new FilterResults();
            results.values = new Currency(CurrencyPopulater.getBASE().getKey(), forResult);
            Logger.show("Source > " + getOriginalSource());
            Logger.show("ForResult >" + forResult);
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            setEditables((Currency) results.values);
        }
    };


}
